<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.6/clipboard.min.js"></script>
<script>new ClipboardJS('.btn');</script>


#List Villager Command 

##Feel free to use

- Log Seller
- Sell All Log
- Buy Dirt, Cobolstone, Rotten Flesh, Spider Eye

<!-- Target -->
<input id="foo" value="/summon villager 0 1 0 
{
    VillagerData:
    {
        profession:farmer,
        level:2,
        type:plains
    },
    Invulnerable:1,
    PersistenceRequired:1,
    Silent:1,
    CustomName:"\"Log Seller\"",
    Offers:
    {
        Recipes:[
            {
                buy:{id:emerald,Count:1},
                sell:{id:acacia_log,Count:10},
                maxUses:9999999
            },
            {
                buy:{id:emerald,Count:1},
                sell:{id:birch_log,Count:10},
                rewardExp:0b,
                maxUses:9999999
            },
            {
                buy:{id:emerald,Count:1},
                sell:{id:dark_oak_log,Count:10},
                rewardExp:0b,
                maxUses:9999999
            },
            {
                buy:{id:emerald,Count:1},
                sell:{id:jungle_log,Count:10},
                rewardExp:0b,
                maxUses:9999999
            },
            {
                buy:{id:emerald,Count:1},
                sell:{id:oak_log,Count:10},
                rewardExp:0b,
                maxUses:9999999
            },
            {
                buy:{id:emerald,Count:1},
                sell:{id:spruce_log,Count:10},
                rewardExp:0b,
                maxUses:9999999
            },
            {
                buy:{id:dirt,Count:64},
                sell:{id:emerald,Count:2},
                rewardExp:0b,
                maxUses:9999999
            },
            {
                buy:{id:cobblestone,Count:64},
                sell:{id:emerald,Count:2},
                rewardExp:0b,
                maxUses:9999999
            },
            {
                buy:{id:rotten_flesh,Count:10},
                sell:{id:emerald,Count:1},
                rewardExp:0b,
                maxUses:9999999
            },
            {
                buy:{id:spider_eye,Count:5},
                sell:{id:emerald,Count:1},
                rewardExp:0b,
                maxUses:9999999
            }
        ]
    }
}">
<!-- Trigger -->
<button class="btn" data-clipboard-target="#foo">
    Copy
</button>

```json
/summon villager 0 1 0 
{
    VillagerData:
    {
        profession:farmer,
        level:2,
        type:plains
    },
    Invulnerable:1,
    PersistenceRequired:1,
    Silent:1,
    CustomName:"\"Log Seller\"",
    Offers:
    {
        Recipes:[
            {
                buy:{id:emerald,Count:1},
                sell:{id:acacia_log,Count:10},
                maxUses:9999999
            },
            {
                buy:{id:emerald,Count:1},
                sell:{id:birch_log,Count:10},
                rewardExp:0b,
                maxUses:9999999
            },
            {
                buy:{id:emerald,Count:1},
                sell:{id:dark_oak_log,Count:10},
                rewardExp:0b,
                maxUses:9999999
            },
            {
                buy:{id:emerald,Count:1},
                sell:{id:jungle_log,Count:10},
                rewardExp:0b,
                maxUses:9999999
            },
            {
                buy:{id:emerald,Count:1},
                sell:{id:oak_log,Count:10},
                rewardExp:0b,
                maxUses:9999999
            },
            {
                buy:{id:emerald,Count:1},
                sell:{id:spruce_log,Count:10},
                rewardExp:0b,
                maxUses:9999999
            },
            {
                buy:{id:dirt,Count:64},
                sell:{id:emerald,Count:2},
                rewardExp:0b,
                maxUses:9999999
            },
            {
                buy:{id:cobblestone,Count:64},
                sell:{id:emerald,Count:2},
                rewardExp:0b,
                maxUses:9999999
            },
            {
                buy:{id:rotten_flesh,Count:10},
                sell:{id:emerald,Count:1},
                rewardExp:0b,
                maxUses:9999999
            },
            {
                buy:{id:spider_eye,Count:5},
                sell:{id:emerald,Count:1},
                rewardExp:0b,
                maxUses:9999999
            }
        ]
    }
}
```